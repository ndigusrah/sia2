<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\supplier;
class SupplierController extends Controller
{
public function index()
    {
        $supplier=\App\Supplier::All();
        return view('admin.supplier.supplier',['supplier'=>$supplier]);
    }
public function create()
    {
        $supp = new \App\Supplier;
        $supp->id=$request->get('addid');
        $supp->nm_supp=$request->get('addnmsup');
        $supp->alamat=$request->get('addalamat');
        $supp->telepon=$request->get('addtelepon');
        return redirect('/supplier');
    }
 public function store(Request $request)
    {
        $save_supplier = new \App\Supplier;
        $save_supplier->id=$request->get('addid');
        $save_supplier->nm_supp=$request->get('addnmsup');
        $save_supplier->alamat=$request->get('addalamat');
        $save_supplier->telepon=$request->get('addtelepon');
        $save_supplier->save();
        return redirect('/supplier');
    }
public function edit($id)
    {
        $supp_edit = \App\Supplier::findOrFail($id);
        return view( 'admin.editSupplier' , ['supplier' => $supp_edit]);
    }

public function update(Request $request, $id)
    {
        $update_supp = \App\Supplier::findOrFail($id);
        $update_supp->id=$request->get('addid');
        $update_supp->nm_supp=$request->get('addnmsupp');
        $update_supp->alamat=$request->get('addalamat');
        $update_supp->telepon=$request->get('addtelepon');
        $update_supp->save();
        //------------- OR you can simply do ---------------------------//

        //$update_supplier = id::findOrFail($id);

        //$update_supplier->update($request->all();

    //-------------------------------------------------------------//
        Alert::success('update', 'Data Berhasil Diupdate');
        return redirect()->route( 'supplier.index');
    }

public function destroy($id)
    {
        $supp=\App\Supplier::findOrFail($id);
        $supp->delete();
        Alert::success('Pesan ','Data berhasil dihapus');
        return redirect()->route('supplier.index');
    }
}

