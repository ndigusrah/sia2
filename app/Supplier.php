<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //jika tidak di definisikan, maka primary akan terdetek id_supp
    protected $primarykey = 'id';
    public $incrementing = false;
    protected $keytype = 'string';
    public $timestamps = false;
    protected $table = "supplier";
    protected $fillable=['id', 'nm_supp', 'alamat', 'telepon'];
}
