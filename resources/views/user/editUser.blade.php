@extends('layouts.layout')
@section('content')
@include('sweetalert::alert')
<form action="{{route('user.update', [$barang->kd_brg])}}" method="POST">
 @csrf
 <input type="hidden" name="_method" value="PUT">
 <fieldset>
        <legend>Input Data Pengguna</legend>
 <div class="form-group row">
 <div class="col-md-5">
 <label for="username">Nama User</label>
 <input class="form-control" type="text" name="username" value="{{$user->username}}" readonly>
 </div>
 <div class="col-md-5">
 <label for="name">Nama Lengkap</label>
 <input class="form-control" type="text" name="name" value="{{user->name}}" readonly>
 </div>